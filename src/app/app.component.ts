import { Component } from '@angular/core';

import { WebsocketService } from './websocket.service';
import { ChatService } from './chat.service';
import { Message } from './message';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [WebsocketService, ChatService]
})
export class AppComponent {
  title = 'app works!';
  message: string;
  author: string;
  response: Message;
  isConnected = false;

  constructor(private chatService: ChatService) {
  }

  connect() {
    this.chatService.startConnection();
    this.chatService.messages.subscribe(msg => {
      console.log(`Response from websocket: { ${msg.author} , ${msg.message} }`);
    });
    this.chatService.messages.subscribe(msg => {
      this.response = msg;
    });
    this.isConnected = true;
  }

  sendMsg() {
    console.log(`new message from client to websocket: ${this.message}`);
    this.chatService.messages.next({ message: this.message, author: this.author });
    this.author = '';
    this.message = '';
  }
}
