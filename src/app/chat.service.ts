import { Injectable } from '@angular/core';
import { Observable, Subject, ReplaySubject, BehaviorSubject } from 'rxjs/Rx';

import { WebsocketService } from './websocket.service';
import { Message } from './message';

const CHAT_URL = 'ws://echo.websocket.org/';

@Injectable()
export class ChatService {

  public messages: BehaviorSubject<Message>;
  public msg: Message;

  constructor(private wsService: WebsocketService) {
    this.messages = new BehaviorSubject({ author: '', message: '' });
  }

  startConnection() {
    this.wsService
      .connect(CHAT_URL)
      .subscribe(response => {
        const data = JSON.parse(response.data);
        this.msg = {
          author: data.author,
          message: data.message
        };
        this.messages.next( {author: data.author,  message: data.message});
      });

/*
      .map((response: MessageEvent): Message => {
        const data = JSON.parse(response.data);
        return {
          author: data.author,
          message: data.message
        };
      });
      */
  }

}
