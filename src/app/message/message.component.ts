import { Component, OnInit } from '@angular/core';

import { ChatService } from '../chat.service';
import { Message } from '../message';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  message: Message;

  constructor(private chatService: ChatService) { }

  ngOnInit() {
    this.chatService.messages.subscribe(msg => {
      this.message = msg;
    });
  }

}
