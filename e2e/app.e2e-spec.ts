import { WsocketPage } from './app.po';

describe('wsocket App', () => {
  let page: WsocketPage;

  beforeEach(() => {
    page = new WsocketPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
